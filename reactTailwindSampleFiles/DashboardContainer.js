import React, { useState, useEffect } from "react";
import total_lead from "../../public/icons/total_leads.svg";
import active_lead from "../../public/icons/active_leads.svg";
import inactive_lead from "../../public/icons/inactive_leads.svg";
import enrolled_students from "../../public/icons/enrolled_students.svg";
import conversion_rate from "../../public/icons/conversion_rate.svg";
import filter_contact from "../../public/icons/filter_contact.svg";
import Link from "next/link";
import ComNetworkModel from "../Network/ComNetworkModel";
import { useDispatch, useSelector } from "react-redux";
import {
  dashboardMemberActivity,
  dashboardTaskRemainder,
  getAllAssigns,
  getGradeCounts,
} from "../../modules/dashbord/dashbordSlice";
import NetworkSelectionModel from "../Contact/NetworkSelectionModel";
import {
  contactListData,
  networkSelection,
} from "../../modules/contact/contactSlice";
import Menu from "../Ui/menu1";

import manage_icon_sidebar from "../../public/icons/manage_icon_sidebar.svg";
import { identifiers } from "../../src/constants/IdentifierConstants";
var networkId;
import {
  fetchFromStorage,
  removeFromStorage,
  saveToStorage,
} from "../../src/context/Storage";
import PipeLineGraph from "./PipeLineGraph";
import EnrollmentGraph from "./EnrollmentGraph";
import UserActivity from "./UserActivity";
import TaskRemainder from "./TaskRemainder";

const DashboardContainer = (props) => {
  let [isOpen, setIsOpen] = useState(false);
  const [campusData, setCampusData] = useState();
  const [graphData, setGraphData] = useState([]);

  const gradeData = useSelector(
    (state) => state.root.dashboard && state.root.dashboard.gradeDataObj
  );
  const allNetworkList = useSelector(
    (state) => state.root?.contact && state?.root?.contact?.allNetwork
  );

  const ContactList = useSelector(
    (state) => state.root?.contact && state.root.contact.contactData
  );
  const [staticData, SetStaticData] = useState();
  const [campusOption, setCampusOption] = useState();
  const [network, setNetwork] = useState();
  const [isFlage, setIsFlage] = useState(false);
  const [selectedCampus, setSelectedCampus] = useState("All Campus");
  const [assing, setassing] = useState([]);
  const [isFilterClear, setIsFilterClear] = useState(true);
  const [selectedAllContact, setAllSelectedContact] = useState(false);
  const [filterData, setFilterData] = useState({
    filter: [],
  });

  const handleFilterByNetwork = (e) => {
    setIsOpen(!isOpen);
    setIsFlage(!isFlage);
  };

  networkId = fetchFromStorage(identifiers.networkId);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(networkSelection());
    dispatch(getAllAssigns(setassing));
    const campus_id = null;
    if (typeof window !== undefined) {
      var networkId = fetchFromStorage(identifiers.networkId);
    }
    dispatch(contactListData(networkId, campus_id, 25, 1, filterData));
  }, []);
  useEffect(() => {
    setCampusData(ContactList?.data?.campusData);
    const filterCampusData = [];
    const filterCampus = [];
    filterCampus =
      ContactList?.data?.campusData &&
      ContactList?.data?.campusData?.map((i) => i.name);
    filterCampusData = filterCampus && ["All Campus", ...filterCampus];
    setCampusOption(filterCampusData);
  }, [ContactList]);
  useEffect(() => {
    setNetwork(allNetworkList?.data);
  }, [allNetworkList?.data?.length]);
  useEffect(() => {
    gradeData &&
      SetStaticData({
        activeLeads: gradeData?.activeLeads,
        conversionRate: gradeData?.conversionRate,
        enrolledStudents: gradeData?.enrolledStudents,
        inactiveLeads: gradeData?.inactiveLeads,
        totalLeads: gradeData?.totalLeads,
        days: gradeData?.days,
      });
    setGraphData(gradeData);
  }, [Object.keys(gradeData).length, gradeData]);
  useEffect(() => {
    const network = fetchFromStorage(identifiers.networkId);
    const CampusItem = campusData?.filter(
      (item) => item.name == selectedCampus
    );
    const campus_id = CampusItem && CampusItem[0]?._id;
    saveToStorage(identifiers.campus_id, campus_id);
    dispatch(getGradeCounts(network, campus_id));
    dispatch(dashboardMemberActivity(network, campus_id));
    dispatch(dashboardTaskRemainder(network, campus_id));
  }, [selectedCampus]);
  const handleSelectCampus = (selectdValue) => {
    setCampus(selectdValue);
  };

  const campus_id = fetchFromStorage(identifiers.campus_id);

  const handleClearFilter = () => {
    setIsFilterClear(!isFilterClear);
    removeFromStorage(identifiers.networkId);
    removeFromStorage(identifiers.networkName);

    if (fetchFromStorage(identifiers.clearFilter) == false) {
      removeFromStorage(identifiers.clearFilter);
    } else {
      saveToStorage(identifiers.clearFilter, false);
    }

    dispatch(getGradeCounts());
  };

  return (
    <>
      <div className="2xl:px-44 md:px-4 px-4 xl:py-11 md:py-4 mb-6 gap-6">
        <div className="flex flex-row items-center justify-between mb-6">
          <div className="flex flex-start gap-2">
            <button
              className="px-4 py-2 text-sm font-semibold rounded-lg items-center flex justify-center gap-2 border bg-white subTitle-color"
              onClick={() => handleFilterByNetwork()}
            >
              <span className="flex">
                <img src={filter_contact.src} alt="" />
              </span>
              <span className="md:flex hidden">
                {" "}
                {fetchFromStorage(identifiers.networkName) ??
                  "Filter by Network"}{" "}
              </span>
            </button>

            {fetchFromStorage(identifiers.clearFilter) == false ? (
              <button
                onClick={handleClearFilter}
                className={`px-4 py-2 text-sm font-semibold rounded-lg items-center flex justify-center gap-2 border bg-white subTitle-color`}
              >
                Clear Filter
              </button>
            ) : null}
          </div>

          {networkId ? (
            <div className="w-44">
              <Menu
                title_character="title_character"
                title="Select Campus"
                label="label"
                option={campusOption}
                selectedOption={selectedCampus}
                setSelectedOption={setSelectedCampus}
                icon={manage_icon_sidebar}
                customClass2="bg-white"
                customClass=""
                text="text"
                handleOptionClick={handleSelectCampus}
              />
            </div>
          ) : null}
        </div>

        <div className="grid lg:grid-cols-5 md:grid-cols-3 grid-cols-2 md:gap-6 sm:gap-4 gap-3 mb-6">
          <div className="w-full bg-white xl:p-6 p-3 rounded-md shadow-sm flex items-center lg:gap-4 gap-3 lg:flex-row flex-col">
            <img src={total_lead.src} alt="Total Leads" className="w-10 h-10" />
            <div className="lg:text-left text-center">
              <h2 className="2xl:text-4xl text-3xl leading-none subTitle-color">
                {staticData?.totalLeads}
              </h2>
              <p className="text-sm leading-none disc-color">Total Leads</p>
            </div>
          </div>
          <div className="w-full bg-white xl:p-6 p-3 rounded-md shadow-sm flex items-center lg:gap-4 gap-3 lg:flex-row flex-col">
            <img
              src={active_lead.src}
              alt="Active Leads"
              className="w-10 h-10"
            />
            <div className="lg:text-left text-center">
              <h2 className="2xl:text-4xl text-3xl leading-none subTitle-color">
                {staticData?.activeLeads}
              </h2>
              <p className="text-sm leading-none disc-color">Active Leads</p>
            </div>
          </div>
          <div className="w-full bg-white xl:p-6 p-3 rounded-md shadow-sm flex items-center lg:gap-4 gap-3 lg:flex-row flex-col">
            <img
              src={inactive_lead.src}
              alt="Inactive Leads"
              className="w-10 h-10"
            />
            <div className="lg:text-left text-center">
              <h2 className="2xl:text-4xl text-3xl leading-none subTitle-color">
                {staticData?.inactiveLeads}
              </h2>
              <p className="text-sm leading-none disc-color">Inactive Leads</p>
            </div>
          </div>
          <div className="w-full bg-white xl:p-6 p-3 rounded-md shadow-sm flex items-center lg:gap-4 gap-3 lg:flex-row flex-col">
            <img
              src={enrolled_students.src}
              alt="Enrolled Students"
              className="w-10 h-10"
            />
            <div className="lg:text-left text-center">
              <h2 className="2xl:text-4xl text-3xl leading-none subTitle-color">
                {staticData?.enrolledStudents}
              </h2>
              <p className="text-sm leading-none disc-color">
                Newly Enrolled Scholars
              </p>
            </div>
          </div>
          <div className="w-full bg-white xl:p-6 p-3 rounded-md shadow-sm flex items-center lg:gap-4 gap-3 lg:flex-row flex-col">
            <img
              src={conversion_rate.src}
              alt="Conversion Rate"
              className="w-10 h-10"
            />
            <div className="lg:text-left text-center">
              <h2 className="2xl:text-4xl text-3xl leading-none subTitle-color">
                {staticData?.conversionRate}
              </h2>
              <p className="text-sm leading-none disc-color">Conversion Rate</p>
            </div>
          </div>
        </div>

        <div className="md:gap-6 sm:gap-4 gap-6 mb-6 dashbord-charts">
          <div className="w-full bg-white rounded-md shadow-sm lg:gap-4 gap-3 mb-6">
            <div className="py-4 md:px-6 px-4 border-b border-zinc-200 flex items-center justify-between">
              <h2 className="md:text-xl text-md font-normal title-color">
                Enrollment Performance
              </h2>
              {networkId ? (
                <p className="md:text-xl text-md font-normal text-zinc-300">
                  {" "}
                  {graphData && graphData?.days} days left{" "}
                </p>
              ) : null}
            </div>
            <div className="md:px-6 px-4 md:pb-6 pb-4 pt-0">
              <EnrollmentGraph graphData={graphData} />
            </div>
          </div>
          <div className="w-full bg-white rounded-md shadow-sm lg:gap-4 gap-3">
            <div className="py-4 md:px-6 px-4 border-b border-zinc-200 flex items-center justify-between">
              <h2 className="md:text-xl text-md font-normal title-color">
                In the Pipeline
              </h2>
              <p></p>
            </div>
            <div className="md:px-6 px-4 md:pb-6 pb-4 pt-4">
              <PipeLineGraph graphData={graphData} />
            </div>
          </div>
        </div>
        <div className="grid lg:grid-cols-2 grid-cols-1 md:gap-6 sm:gap-4 gap-6 mb-6">
          <TaskRemainder campus_id={campus_id} assing1={assing} />
          {/* <div style={{ color: "#e5e7eb" }}>.</div> */}
          <div>
            <UserActivity campus_id={campus_id} />
          </div>
        </div>

        <NetworkSelectionModel
          isOpen={isOpen}
          setIsOpen={setIsOpen}
          networkList={network}
          setNetwork={setNetwork}
          isFlage={isFlage}
          setIsFlage={setIsFlage}
          setSelectedCampus={setSelectedCampus}
          setIsFilterClear={setIsFilterClear}
          setAllSelectedContact={setAllSelectedContact}
        />
      </div>
    </>
  );
};

export default DashboardContainer;
