import React, { useEffect, useRef } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";
import close_icon from "../../public/icons/close-icon.svg";
import delete_icon from "../../public/icons/delete_contact.svg";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import moment from "moment";
import ConfirmationDeleteModal from "../ConfirmationModal/ConfirmationDeleteModal";

import {
  contactMerge,
  deleteContacts,
  getAllDuplicates,
  studentContactMerge,
} from "../../modules/setting/DuplicateDataSlice";
import {
  saveToStorage,
  fetchFromStorage,
  removeFromStorage,
} from "../../src/context/Storage";
import { identifiers } from "../../src/constants/IdentifierConstants";
import { Checkbox } from "@mui/material";
import ConfirmationMessage from "../ConfirmationModal/ConfirmationMessage";
import Loader from "../../components/Loader/Loader";
import { useSelector } from "react-redux";
import deleteicon from "../../public/icons/ic_baseline-delete.svg";
import Link from "next/link";

const DataModel = ({
  isOpen,
  setIsOpen,
  duplicateData,
  setDuplicateData,
  message,
  contactId,
  isScholar,
  selectedOp,
  networkid,
  isLoading,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [removedData, setRemovedData] = useState([]);
  const [isFlag, setIsFlag] = useState(false);
  const [isRadioButton, setIsRadioButton] = useState(false);
  const [isCheckboxButton, setIsCheckboxButton] = useState(false);
  const [isConfirmationDeleteOpen, setIsConfirmationDeleteModal] =
    useState(false);

  const [Clicked, setClicked] = useState({});
  const [masterRecord, setMasterRecord] = useState(null);
  const [meargeRecord, setmeargeRecord] = useState(null);
  const [deleteData, setDeleteData] = useState();
  const [confirmationMergedMessage, setConfirmationMergedMessage] =
    useState(false);
  const [mergedid, setmergedId] = useState();
  const [otherName, setOtherName] = useState(null);
  const [isviewDetail, setIsviewDetail] = useState();
  const [isRemoveContact, setIsRemoveContact] = useState(false);
  const [removedId, setRemovedId] = useState(null);
  const [Count, setCount] = useState();
  const [removedDuplicateData, setremovedDuplicateData] = useState();
  const [number, setNumber] = useState([]);

  useEffect(() => {
    if (fetchFromStorage(identifiers.selectedduplicate) == 0) {
      setTimeout(() => {
        setIsOpen(false);
        removeFromStorage(identifiers.selectedduplicate);
      }, 100);
    }

    setCount(
      duplicateData[contactId !== null && contactId !== undefined && contactId]
        ?.count
    );

    duplicateData[contactId !== null && contactId !== undefined && contactId]
      ?.count == null ||
    duplicateData[contactId !== null && contactId !== undefined && contactId]
      ?.count == undefined
      ? setIsOpen(false)
      : null;
  }, []);

  useEffect(() => {
    dispatch(getAllDuplicates(networkid));
  }, [dispatch, networkid]);

  let idArr = fetchFromStorage(identifiers?.removedDuplicates);
  var arrOfData = [];
  if (idArr && idArr.length > 0) {
    arrOfData = duplicateData[contactId !== null && contactId].data.filter(
      (item) => !idArr.includes(item.id)
    );
  } else {
    if (duplicateData[contactId !== null && contactId]?.data) {
      arrOfData = duplicateData[contactId !== null && contactId].data;
    }
  }

  const handleCancelButton = () => {
    removeFromStorage(identifiers.isOpen);
    removeFromStorage(identifiers.contactIds);
    removeFromStorage(identifiers.removedDuplicates);
    removeFromStorage(identifiers.selectedOp);
    removeFromStorage(identifiers.selectedduplicate);
    setIsOpen(false);
  };
  const handleSaveButton = () => {
    setIsConfirmationModal(!isConfirmationOpen);
  };

  const handleChange = (e, id) => {
    setIsCheckboxButton(!isCheckboxButton);
    setIsRadioButton(false);
    isCheckboxButton === true ? setMasterRecord(null) : setMasterRecord(id);
    if (isCheckboxButton === false) {
      changeMasterRecord();
    }
    if (isRadioButton === true) {
      var radio = document.getElementById(`${meargeRecord}`);
      if (radio && radio.checked === true) {
        radio.checked = false;
      }
    }
  };

  const handleRadioChange = (e, id) => {
    setIsRadioButton(true);
    setmeargeRecord(id);
  };

  const changeMasterRecord = () => {
    if (isRadioButton === true && isCheckboxButton === false) {
      setMasterRecord(null);
      setmeargeRecord(null);
    }
    if (isCheckboxButton === true) {
      setmeargeRecord(null);
      var checkbox = document.getElementById(`${meargeRecord}`);
      if (checkbox.checked === true) {
        checkbox.checked = false;
      }
    }
  };
  const handleRemoveContactCard = (id) => {
    setClicked((prevState) => ({
      ...Clicked,
      [removedId]: !Clicked[removedId], // <-- toggle clicked boolean
    }));

    saveToStorage(identifiers.removedData, Clicked);

    const updatedCarsArray = [...number, id];
    setNumber(updatedCarsArray);
    saveToStorage(identifiers.removedDuplicates, updatedCarsArray);
  };

  const handleMergeContact = () => {
    isScholar == true
      ? dispatch(
          studentContactMerge(masterRecord, meargeRecord, setIsCheckboxButton)
        )
      : dispatch(contactMerge(mergedid, setIsCheckboxButton));
  };

  const handleDeleteContact = () => {
    dispatch(deleteContacts(deleteData));
  };
  const handleCancel = () => {
    setIsConfirmationDeleteModal(false);
  };
  const handleOpenConfirmationModal = (id) => {
    setIsConfirmationDeleteModal(true);
    setDeleteData(id);
    setCount(Count - 1);
  };

  const handleRemoveContactConfirmationMergedMessage = (id) => {
    setIsRemoveContact(!isRemoveContact);
    setRemovedId(id);
    setCount(Count - 1);
  };

  const handleOpenConfirmationMergedMessage = (data) => {
    const mergedId = data?.filter((item) => item?.id == masterRecord);
    const OtherId = data?.filter((item) => item?.id !== masterRecord);

    const Mdata = {
      masterId: mergedId[0]?.id,
      otherId: OtherId[0]?.id,
    };
    const otherDetails = {
      firstName: OtherId[0]?.firstName,
      lastName: OtherId[0]?.lastName,
    };

    setOtherName(otherDetails);
    setConfirmationMergedMessage(true);
    setmergedId(Mdata);
    if (data.length > 0) {
      saveToStorage(identifiers.selectedduplicate, 0);
    }
  };

  return (
    <>
      <Dialog
        as="div"
        open={isOpen}
        className="fixed md:max-w-lg w-full z-30 bg-white shadow-lg"
        onClose={() => handleCancelButton()}
      >
        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 mb-6 pb-4 text-center bg-slate-900/10">
            <Dialog.Panel className="w-full max-w-[50%] custom-scroll transform overflow-hidden rounded-2xl bg-white text-left align-middle shadow-xl transition-all">
              <div className="px-6 py-4 bg-white sticky top-0 flex items-center justify-between z-30 border-b border-gray-200">
                <Dialog.Title
                  as="h3"
                  className="text-xl font-normal title-color"
                >
                  {message}
                </Dialog.Title>
                <button
                  onClick={() => {
                    handleCancelButton();
                  }}
                  className="w-8 h-8 rounded-lg bg-gray-100 p-1 flex items-center justify-center cursor-pointer outline-none"
                >
                  <img src={close_icon.src} alt="Close" />
                </button>
              </div>
              <div className="mt-3 px-6 mb-5">
                {masterRecord !== null && Count !== 1 ? (
                  <p className="text-gray-500">
                    Note: Select another contact to merge with
                  </p>
                ) : (
                  <p className="text-gray-500">
                    Note: Select the master record to merge with
                  </p>
                )}
              </div>
              {
                <>
                  <div
                    className={
                      Count !== undefined && Count <= 3
                        ? `px-6 flex justify-center flex-row gap-x-[18px] overflow-x-auto overflow-y-hidden pb-5`
                        : `px-6 flex  flex-row gap-x-[18px] overflow-x-auto overflow-y-hidden pb-5`
                    }
                  >
                    {arrOfData &&
                      arrOfData.map((innerArr, index2) => {
                        return Clicked[innerArr?.id] ? null : (
                          <div
                            className={`flex flex-col shadow-md content-between my-4 cursor-pointer w-[185px] min-w-[186px] rounded-[4px]`}
                          >
                            {/* whene close icon show use below html */}
                            <div
                              onClick={() =>
                                handleRemoveContactConfirmationMergedMessage(
                                  innerArr?.id
                                )
                              }
                              className="flex justify-end border-b border-gray-200 py-3 px-3 w-full"
                            >
                              <img src={close_icon.src} alt="Close" />
                            </div>
                            {/* end close html */}
                            <div className="flex flex-col justify-between px-6 py-6 h-full">
                              <div className="flex flex-row justify-between w-full">
                                {isCheckboxButton &&
                                masterRecord !==
                                  (innerArr?._id
                                    ? innerArr?._id
                                    : innerArr?.id) ? (
                                  <input
                                    type="radio"
                                    name="student"
                                    id={`${
                                      innerArr?._id
                                        ? innerArr?._id
                                        : innerArr?.id
                                    }`}
                                    onChange={(e) =>
                                      handleRadioChange(
                                        e,
                                        innerArr?._id
                                          ? innerArr?._id
                                          : innerArr?.id
                                      )
                                    }
                                    className={`${
                                      innerArr?._id
                                        ? innerArr?._id
                                        : innerArr?.id
                                    } form-check-input h-4 w-4 border border-gray-300 rounded checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer bg-white`}
                                  />
                                ) : (
                                  <input
                                    type="checkbox"
                                    name="student"
                                    id={`${
                                      innerArr?._id
                                        ? innerArr?._id
                                        : innerArr?.id
                                    }`}
                                    onChange={(e) =>
                                      handleChange(
                                        e,
                                        innerArr?._id
                                          ? innerArr?._id
                                          : innerArr?.id
                                      )
                                    }
                                    className={`${
                                      innerArr?._id
                                        ? innerArr?._id
                                        : innerArr?.id
                                    } form-check-input h-4 w-4 border border-gray-300 rounded checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer bg-white`}
                                  />
                                )}

                                <img
                                  onClick={() =>
                                    handleOpenConfirmationModal(innerArr?.id)
                                  }
                                  src={deleteicon.src}
                                />
                              </div>
                              <div className="flex flex-col items-center h-100 justify-center">
                                <div className="rounded-full bg-slate-200 h-[50px] w-[50px] flex justify-center items-center mb-2">
                                  <span className="font-semibold text-stone-900 text-[16px]">
                                    {" "}
                                    {innerArr?.firstName
                                      ?.charAt(0)
                                      .toUpperCase()}
                                    {innerArr?.lastName
                                      ?.charAt(0)
                                      .toUpperCase()}{" "}
                                  </span>
                                </div>
                                <div className="flex flex-col justify-items-center items-center mb-2">
                                  <p className="font-normal text-center text-[16px] subTitle-color ">
                                    {innerArr?.firstName} {innerArr?.lastName}
                                  </p>
                                  <p className="font-normal text-[16px] subTitle-color">
                                    {innerArr?.phoneNumber
                                      ? `${"#" + innerArr?.phoneNumber}`
                                      : innerArr?.email
                                      ? `${innerArr?.email}`
                                      : null}
                                  </p>
                                  <p className="font-normal text-[12px] text-color-a4a4">
                                    {" "}
                                    Created on:
                                    {moment(innerArr?.createdAt).format(
                                      "MM/DD/YYYY"
                                    )}
                                  </p>
                                </div>
                                <div
                                  className="flex justify-center"
                                  style={{
                                    borderWidth: "1px",
                                    borderColor: "#00828A",
                                    color: "#00828A",
                                    borderRadius: "8px",
                                    fontWeight: "600",
                                    paddingTop: "0.25rem",
                                    paddingBottom: "0.25rem",
                                    paddingLeft: "0.75rem",
                                    paddingRight: "0.75rem",
                                  }}
                                >
                                  <Link
                                    className="px-3 py-1 text-sm font-semibold rounded-[8px] items-center flex justify-center gap-2 border primary-border bg-white primary-color"
                                    href={{
                                      pathname: `/contact/${innerArr?.id}`,
                                      query: {
                                        isContact: true,
                                        contactIds: contactId,
                                        selectedOp: selectedOp,
                                      },
                                    }}
                                  >
                                    View Details
                                  </Link>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  <div className="p-3 bg-white   w-full border-t border-gray-200 flex  items-center justify-center md:gap-4 gap-3 flex-wrap z-10">
                    <button
                      onClick={() => {
                        isRadioButton == true && isCheckboxButton == true
                          ? handleOpenConfirmationMergedMessage(arrOfData)
                          : null;
                      }}
                      className={
                        isRadioButton == true && isCheckboxButton == true
                          ? `px-4 py-2 text-sm font-normal rounded-lg items-center flex justify-center border primary-bg bg-light text-white `
                          : ` px-4 py-2 text-sm font-normal rounded-lg items-center flex justify-center border bg-light text-white bg-light-gray-dce1e1`
                      }
                    >
                      Merge Contacts
                    </button>
                  </div>
                </>
              }
            </Dialog.Panel>
          </div>
        </div>
        {isLoading && <Loader />}
      </Dialog>

      <ConfirmationMessage
        handleOpenConfirmationModals={
          handleRemoveContactConfirmationMergedMessage
        }
        isConfirmationOpen={isRemoveContact}
        setIsConfirmationModal={setIsRemoveContact}
        header_title="Remove Contact"
        text={`Are you sure you want to remove this contact from 
            merging? It will just remove the contact from merging, 
            but will stay as duplicate contact until you merge it`}
        handleCloseModal={() => handleCancel()}
        handleYes={() => {
          handleRemoveContactCard(removedId);
        }}
        id={duplicateData[contactId !== null && contactId]?._id}
      />

      <ConfirmationDeleteModal
        handleOpenConfirmationModal={handleOpenConfirmationModal}
        isConfirmationDeleteOpen={isConfirmationDeleteOpen}
        setIsConfirmationDeleteModal={setIsConfirmationDeleteModal}
        header_title="Delete Contact"
        text="Are you sure want to delete?"
        handleCloseModal={() => handleCancel()}
        handleYes={handleDeleteContact}
      />
      <div>
        {confirmationMergedMessage == true ? (
          <ConfirmationMessage
            handleOpenConfirmationModals={handleOpenConfirmationMergedMessage}
            isConfirmationOpen={confirmationMergedMessage}
            setIsConfirmationModal={setConfirmationMergedMessage}
            header_title="Merge Contact"
            text={
              Count && Count == 2
                ? `Do you want to merge ${otherName?.firstName} ${otherName?.lastName}
            to Master Contact?`
                : `Do you want to merge ${otherName?.firstName} ${otherName?.lastName}
            to Master Contact? Other contacts
            remain as duplicates`
            }
            handleCloseModal={() => handleCancel()}
            handleYes={handleMergeContact}
          />
        ) : null}
      </div>
      {/* </Transition> */}
    </>
  );
};

export default DataModel;
