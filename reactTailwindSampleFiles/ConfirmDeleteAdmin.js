import React from "react";
import { Dialog } from "@headlessui/react";

const ConfirmDeleteAdmin = ({
  handleOpenConfirmationModal2,
  isConfirmationDeleteOpen,
  setIsConfirmationDeleteModal,
  header_title,
  text,
  handleYes,
  // handleCloseContactModal,
  handleCloseModal,
}) => {
  const handleCancelButton = () => {
    setIsConfirmationDeleteModal(!isConfirmationDeleteOpen);
  };
  const handleSaveButton = () => {
    handleYes();
    handleCancelButton();
  };

  return (
    <>
      <Dialog
        as="div"
        open={isConfirmationDeleteOpen}
        className="relative z-50"
        onClose={() => handleCancelButton()}
      >
        <div className="fixed inset-0 bg-black bg-opacity-25" />
        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center bg-slate-900/10">
            <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-white text-left align-middle shadow-xl transition-all">
              <div className="px-3 py-5">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  {header_title}
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">{text}</p>
                </div>
              </div>

              <div className="p-3 bg-white md:max-w-md w-full border-t border-gray-200  flex items-center justify-end md:gap-4 gap-3 flex-wrap z-10">
                <button
                  onClick={() => handleCloseModal()}
                  className="px-4 py-2 text-sm font-semibold rounded-lg items-center flex justify-center gap-2 border bg-white subTitle-color"
                >
                  No
                </button>
                <button
                  onClick={() => handleSaveButton()}
                  className="px-4 py-2 text-sm font-semibold rounded-lg items-center flex justify-center gap-2 text-white primary-bg"
                >
                  Yes
                </button>
              </div>
            </Dialog.Panel>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default ConfirmDeleteAdmin;
